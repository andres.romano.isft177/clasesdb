### Materia: Bases de Datos

Esta configuracion de docker es la que utilizaremos para
los ejemplos que veamos en clase, para que todos tengamos 
lo mismo y evitar errores.

> Esta compuesto por MySQL, PhpMyAdmin, PHP 7.4 y Apache2

### Contenedores

+ mysql: Contiene la version 5.7 del motor MySQL
+ myadm: Administrador de Bases de Datos PhpMyAdmin
+ html: Aqui hay que poner nuestro proyecto web, contiene PHP y Apache
